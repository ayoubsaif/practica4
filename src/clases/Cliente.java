package clases;

public class Cliente {
	
	// atributos
	private String nombreCliente;
	private String apellidoCliente;
	private String sexo;
	private double peso;
	private int altura;
	private String gym;

	//constructor
	public Cliente (String nombreCliente) {
		this.nombreCliente=nombreCliente;
	}
	
	// SETTER y GETTER
	
	//		NOMBRE
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	
	//		APELLIDO
	public String getApellidoCliente() {
		return apellidoCliente;
	}
	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}
	
	//		PESO
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	//		SEXO
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	//		ALTURA
	public double getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}
	
	//		CENTRO DE GYM
	public String getGym() {
		return gym;
	}
	public void setGym(String gym) {
		this.gym = gym;
	}
	
	//metodo toString
	@Override
	public String toString() {
		return "Cliente [nombreCliente=" + nombreCliente + ",apellidoCliente=" + apellidoCliente + ", sexo=" + sexo + ", peso=" + peso + ", altura=" + altura + ", gym="+ gym + "]";
	}
}
