package clases;

public class Gym {
	// atributos
	// declaracion
	private Cliente[] clientes;

	// crear un vector de String
	// String[] miVector = new String[3];
	// declaracion -> se coloca al comienzo de la clase
	// String[] miVector;
	// asignaci�n -> se coloca en el constructor
	// miVector=new String[3];

	// constructor
	public Gym(int maxClientes) {
		// asignacion
		this.clientes = new Cliente[maxClientes];
	}

	// alta Cliente
	public void altaCliente(String nombreCliente, String apellidoCliente,double peso, String sexo, int altura, String gym) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] == null) {
				clientes[i] = new Cliente(nombreCliente);
				clientes[i].setApellidoCliente(apellidoCliente);
				clientes[i].setPeso(peso);
				clientes[i].setSexo(sexo);
				clientes[i].setAltura(altura);
				clientes[i].setGym(gym);
				break;
			}
		}
	}

	// buscar Cliente
	public Cliente buscarCliente(String nombreCliente, String apellidoCliente) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getNombreCliente().equals(nombreCliente) 
							&& clientes[i].getApellidoCliente().equals(apellidoCliente)) {
					return clientes[i];
				}
			}
		}
		return null;
	}

	// eliminar Clientes
	public void eliminarCliente(String nombreCliente) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getNombreCliente().equals(nombreCliente)) {
					clientes[i] = null;
				}
			}
		}
	}

	// listar clientes
	public void listarClientes() {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				System.out.println(clientes[i]);
			}
		}
	}
	
	//cambiar nombre Cliente
	public void cambiarNombreCliente(String nombreCliente, String nombreCliente2) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getNombreCliente().equals(nombreCliente)) {
					clientes[i].setNombreCliente(nombreCliente2);
				}
			}
		}
	}
	
	//cambiar Peso Cliente
	public void cambiarPesoCliente(String nombreCliente, int pesoCliente) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getNombreCliente().equals(nombreCliente)) {
					clientes[i].setPeso(pesoCliente);
				}
			}
		}
	}
	
	//listar clientes por zoo
	public void listarClientesPorGym(String nombreGym) {
		for (int i = 0; i < clientes.length; i++) {
			if (clientes[i] != null) {
				if (clientes[i].getGym().equals(nombreGym)) {
					System.out.println(clientes[i]);
				}
			}
		}
	}
}
