package programa;

import clases.Gym;
import java.util.Scanner;
public class Programa {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Creando Instancia Gym...");
		int maxClientes=20;
		Gym miGym = new Gym(maxClientes);
		System.out.println("Instancia Creada");

		System.out.println("	  _______     ____  __ ");
		System.out.println("	 / ____\\ \\   / /  \\/  |");
		System.out.println("	| |  __ \\ \\_/ /| \\  / |");
		System.out.println("	| | |_ | \\   / | |\\/| |");
		System.out.println("	| |__| |  | |  | |  | |");
		System.out.println("	 \\_____|  |_|  |_|  |_|");

		// Declaramos variable Menu
		int opcion = 0;
		
		// Bucle del menu
		while(opcion!=9) {

		System.out.println("\n1.- Crear 10 clientes");
		System.out.println("2.- Listar clientes");
		System.out.println("3.- Buscar un cliente por su nombre");
		System.out.println("4.- Eliminar Cliente");
		System.out.println("5.- Almacenamos un nuevo Cliente");
		System.out.println("6.- Modificar el nombre de un Cliente");
		System.out.println("7.- Modificar el peso de un Cliente");
		System.out.println("8.- Listar clientes de un Gimnasio");
		System.out.println("9.- Cerrar menu");
		System.out.println("\nSeleccione una opción:");
		
		opcion = input.nextInt();
			switch(opcion){
				case 1 :
					System.out.println("### Creando clientes ... ###");
					// 				(nombreCliente,peso,sexo,altura,gym)
					miGym.altaCliente("Javier",	"Martinez",	80.00, "Masculino", 165, "GYM Centro");
					miGym.altaCliente("Maria",	"Rodriguez",90.00, "Femenino", 	180, "GYM Actur");
					miGym.altaCliente("Carlos",	"Lopez", 	100.00,"Masculino",	175, "GYM Centro");
					miGym.altaCliente("Lara",	"Tomás", 	90.00, "Femenino", 	185, "GYM Casablanca");
					miGym.altaCliente("Nuria",	"Martinez", 110.00,"Femenino", 	165, "GYM Centro");
					miGym.altaCliente("Laura",	"Lopez", 	80.00, "Femenino", 	180, "GYM Romareda");
					miGym.altaCliente("Carlos",	"Gimenez", 	70.00, "Masculino", 180, "GYM Romareda");
					miGym.altaCliente("Jarvier","Lopez", 	85.00, "Masculino", 180, "GYM Actur");
					miGym.altaCliente("Lara",	"Lopez", 	90.00, "Masculino", 180, "GYM Casablanca");
					miGym.altaCliente("Jorge",	"Ferrer", 	90.00, "Masculino", 180, "GYM Casablanca");
					System.out.println("### 10 Clientes creados ... ###");
					miGym.listarClientes();
					break;
				case 2 :
					System.out.println("2.- Listar clientes");
					miGym.listarClientes();
					break;
				case 3 :
					input.nextLine();
					System.out.println("3.- Buscar un cliente por su nombre y apellido");
						System.out.println("Nombre:");
							String nombre4=input.nextLine();
						System.out.println("Apellido:");
							String apellido4=input.nextLine();
					
					System.out.println(miGym.buscarCliente(nombre4,apellido4));
					break;
				case 4 :
					System.out.println("4.- Eliminar Cliente");
					System.out.println("Introduzca el nombre del cliente a eliminar:");
						String nombre5=input.nextLine();
					miGym.eliminarCliente(nombre5);
					miGym.listarClientes();
					
					break;
				case 5 :
					input.nextLine();
					System.out.println("5.- Creamos un nuevo Cliente");
					System.out.println("Introduzca los datos del cliente:");
						System.out.println("Nombre:");
							String nombre=input.nextLine();
						System.out.println("Apellido:");
							String apellido=input.nextLine();
						System.out.println("Peso:");
							double peso=input.nextDouble();
						input.nextLine();
						System.out.println("Sexo:");
							String sexo=input.nextLine();
						System.out.println("Altura (cm):");
							int altura=input.nextInt();
						input.nextLine();
						System.out.println("GYM:");
							String gym=input.nextLine();
						
						miGym.altaCliente(nombre, apellido, peso, sexo, altura, gym);
						System.out.println("## Cliente creado: ##");
						System.out.println(miGym.buscarCliente(nombre,apellido));
					break;
				case 6 :
					System.out.println("6.- Modificar el nombre de un Cliente");
						miGym.cambiarNombreCliente("Dora", "Matilda");
						miGym.listarClientes();
					break;
				case 7 :
					System.out.println("7.- Modificar el peso de un Cliente");
						miGym.listarClientes();
						System.out.println("Introduce el nombre de un cliente:");
						String nombre1 = input.nextLine();
						System.out.println("Introduce el nuevo peso de: "+nombre1);
						int peso1 = input.nextInt();
						input.nextLine();
						miGym.cambiarPesoCliente(nombre1, peso1);
						miGym.listarClientes();
					break;
				case 8 :
					System.out.println("8.- Listar clientes de un Gimnasio");
						System.out.println("Escriba el nombre del gimnasio:");
						String gimnasio = input.nextLine();
						
						miGym.listarClientesPorGym(gimnasio);
					break;
				case 9 :
					System.out.println("Hasta pronto...");
					break;
		
				default:
			}
		}
		input.close();
	}
}
